import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { SideMenuComponent } from './side-menu/side-menu.component';
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { ActionManagerComponent } from './action-manager/action-manager.component';
import { TriggerComponent } from './trigger/trigger.component';

const routes: Routes = [
  {
    path: "",
    component: LoginComponent,
    pathMatch: "full"
  },
  {
    path: "side-menu",
    component: SideMenuComponent,
    pathMatch: "full"
  },
  {
    path: "dashboard",
    component: DashboardContentComponent,
    pathMatch: "full"
  },
  {
    path: "action-manager",
    component: ActionManagerComponent,
    pathMatch: "full"
  },
  {
    path: "trigger",
    component: TriggerComponent,
    pathMatch: "full"
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
