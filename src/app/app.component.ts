import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, NavigationEnd, Event } from "@angular/router";
import { filter, map, mergeMap } from "rxjs/operators";
import { from } from "rxjs";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'minaCMS';
  currentUrl: string;
  test: boolean = false;
  constructor(
    private router: Router,
    private Ar:ActivatedRoute ,
    private route: ActivatedRoute
  ) {
    this.router.events.subscribe((event : any) => {
        this.currentUrl = this.router.url;
        this.test = this.currentUrl === "" || this.currentUrl === "/" ? false : true
        console.log(this.currentUrl);
    });
    
    
  }
  ngOnInit() {
    this.currentUrl = ""
    this.test = this.currentUrl === "" || this.currentUrl === "/" ? false : true
    
    
    //this.currentUrl = this.router.url;
    // this.router.events
    //   .pipe(
    //     filter(event => event instanceof NavigationEnd),
    //     map(() => this.route),
    //     map(route => {
    //       while (route.firstChild) route = route.firstChild;
    //       return route;
    //     }),
    //     filter(route => route.outlet === "primary"),
    //     mergeMap(route => route.data)
    //   )
    //   .subscribe(event => {
    //     this._seoService.updateTitle(event["title"]);
    //     //Updating Description tag dynamically with title
    //     this._seoService.updateDescription(
    //       event["title"] + event["description"]
    //     );
    //     this._seoService.createLinkForCanonicalURL(); // add canonical link in angular dynamically
        
        
    //   });
   
  }
}
