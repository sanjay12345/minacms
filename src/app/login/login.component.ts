import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit() {
    window.onbeforeunload = function() {
      window.scrollTo(0, 0);
    };
  }

  //? PAUSE PLAYER
  pausePlayer() {
    console.log("click");
    
  }
  login(){
    this.router.navigate(["/dashboard"]);
  }

}
