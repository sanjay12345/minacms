import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-action-manager',
  templateUrl: './action-manager.component.html',
  styleUrls: ['./action-manager.component.scss']
})
export class ActionManagerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
  };
} 


}
