import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-content',
  templateUrl: './dashboard-content.component.html',
  styleUrls: ['./dashboard-content.component.scss']
})
export class DashboardContentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
  }
}

}
